

class No:
    def __init__(self, data):
        self.data = data
        self.proximo = None


class ListaEncadeada:
    def __init__(self):
        self.cabeca = None
        self._tamanhoLista = 0


    def adicionaFinal(self, elemento):
        if self.cabeca:
            ponteiro = self.cabeca
            while(ponteiro.proximo):
                ponteiro = ponteiro.proximo
            ponteiro.proximo = No(elemento)

        else:
            #primeira inserção
            self.cabeca = No(elemento)
        self._tamanhoLista = self._tamanhoLista + 1


    #retorna o tamanho da lista
    def __len__(self):
        return self._tamanhoLista


    def _getNo(self, index):
        ponteiro = self.cabeca
        for i in range(index):
            if ponteiro:
                ponteiro = ponteiro.proximo
            else:
                raise IndexError("erro na busca")
        return ponteiro


    def __getitem__(self, index):
        ponteiro = self._getNo(index)
        if ponteiro:
            return ponteiro.data
        raise IndexError("erro na busca")


    def __setitem__(self, index, elemento):
        ponteiro = self._getNo(index)
        if ponteiro:
            ponteiro.data = elemento
        else:
            raise IndexError("erro na busca")

    #retirna o incice da lista
    def index(self, elemento):
        ponteiro = self.cabeca
        i = 0
        while(ponteiro):
            if ponteiro.data == elemento:
                return i
            ponteiro = ponteiro.proximo
            i = i+1
        raise ValueError("Não esta na lista")

    def inserir(self, index, elemento):
        if index == 0:
            no = No(elemento)
            no.proximo = self.cabeca
            self.cabeca = no
        else:
            ponteiro = self._getNo(index - 1)
            no = No(elemento)
            no.proximo = ponteiro.proximo
            ponteiro.proximo = no
        self._tamanhoLista = self._tamanhoLista + 1


    def remover(self, elemento):
        if self.cabeca == None:
            raise ValueError("Não esta na lista")

        elif self.cabeca.data == elemento:
            self.cabeca = self.cabeca.proximo
            self._tamanhoLista = self._tamanhoLista - 1
            return True
        else:
            antecessor = self.cabeca
            ponteiro = self.cabeca.proximo
            while(ponteiro):
                if ponteiro.data == elemento:
                    antecessor.proximo = ponteiro.proximo
                    ponteiro.proximo = None
                antecessor = ponteiro
                ponteiro = ponteiro.proximo
            self._tamanhoLista = self._tamanhoLista - 1
            return True
        raise ValueError("Não esta na lista")



#Funções para poder printar a listas

    def __repr__(self):
        variavelNo = ""
        ponteiro = self.cabeca
        while(ponteiro):
            variavelNo = variavelNo + str(ponteiro.data) + " -> "
            ponteiro = ponteiro.proximo
        return variavelNo


    def __str__(self):
        return self.__repr__()



